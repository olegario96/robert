# Built-in imports
from datetime import datetime

# External imports
from freezegun import freeze_time

# Personal imports
from robert.utils.date_helper import get_current_year_month_day, \
    is_a_working_day_in_brazil, \
    is_today_first_working_day_in_brazil


@freeze_time('2020-02-18')
def test_get_current_year_month_day() -> None:
    year_month_day: dict = get_current_year_month_day()
    assert year_month_day.get('year') == 2020
    assert year_month_day.get('month') == 2
    assert year_month_day.get('day') == 18


@freeze_time('2020-02-18')
def test_is_a_working_day_in_brazil_for_a_working_date():
    mock_date: datetime = datetime.now()
    assert is_a_working_day_in_brazil(mock_date)


@freeze_time('2020-02-16')
def test_is_a_working_day_in_brazil_for_a_not_working_date():
    mock_date: datetime = datetime.now()
    assert not is_a_working_day_in_brazil(mock_date)


@freeze_time('2020-02-02')
def test_is_today_first_working_day_in_brazil_for_not_working_date():
    assert not is_today_first_working_day_in_brazil()


@freeze_time('2020-02-03')
def test_is_today_first_working_day_in_brazil_for_first_working_date():
    assert is_today_first_working_day_in_brazil()


@freeze_time('2020-02-04')
def test_is_today_first_working_day_in_brazil_for_not_first_working_date():
    assert not is_today_first_working_day_in_brazil()
