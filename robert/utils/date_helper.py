# Built-in imports
from datetime import datetime

# External imports
from workalendar.america import Brazil


def get_current_year_month_day() -> dict:
    current_date: datetime = datetime.now()
    year_month_day_str: str = current_date.strftime('%Y-%m-%d')
    year_month_day_list: list = list(map(int, year_month_day_str.split('-')))
    return {
        'year': year_month_day_list[0],
        'month': year_month_day_list[1],
        'day': year_month_day_list[2],
    }


def is_a_working_day_in_brazil(date: datetime) -> bool:
    calendar: Brazil = Brazil()
    return calendar.is_working_day(date)


# Disabling R1710 rule for this function because it always return a bool
def is_today_first_working_day_in_brazil() -> bool: # pylint: disable=R1710
    # The hour doesn't make in any difference for our use case
    current_date: datetime = datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)
    if not is_a_working_day_in_brazil(current_date):
        return False

    month, year = int(current_date.month), int(current_date.year)
    days_in_month: list = list(i for i in range(1, 32))
    for day in days_in_month:
        date_build: datetime = datetime(year, month, day)
        if is_a_working_day_in_brazil(date_build):
            return date_build == current_date
